# Changelog
All notable changes to this job will be documented in this file.

## [1.2.0] - 2022-10-28
* Add `GOLINT_OUTPUT_FORMAT` variable to export the report as `junit` or `code-climate` artifact
* Fix the Junit report artifact

## [1.1.0] - 2022-06-13
* Add docker image tag in variable 

## [1.0.0] - 2022-04-14
* Change the default stage into `tests`

## [0.1.0] - 2022-04-04
* Initial version
