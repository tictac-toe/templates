# Changelog
All notable changes to this job will be documented in this file.

## [1.0.0] - 2022-12-06
* Fix the dry-run mode
* Allow to specify full configuration path
* Update versions of all tools used in the job
* BREAKING CHANGE: update some variables names to be compliant with another jobs

## [0.4.0] - 2022-09-20
* Run this job only if previous stages are successful

## [0.3.0] - 2022-06-13
* Add docker image tag in variable

## [0.2.0] - 2022-05-20
* Update image to node:18-buster

## [0.1.0] - 2021-06-14
* Initial version
