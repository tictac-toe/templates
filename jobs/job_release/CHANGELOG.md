# Changelog
All notable changes to this job will be documented in this file.

## [1.0.0] - 2022-11-23
* Allow to fetch jobs from nested directories
* Use snippet maintained by R2Devops instead of external resource

## [0.1.0] - 2022-10-14
* Initial version
